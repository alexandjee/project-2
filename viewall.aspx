﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="viewall.aspx.cs" Inherits="viewall" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="global/bootstrap.min.css" rel="stylesheet" />
    <link href="global/Style.css" rel="stylesheet" />
    <title>Project 2</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <h1>Project 2</h1>
        <br />
    <a href="Default.aspx">Home</a>|<a href ="#">View all Processed Budgets</a>

<br />
        <br />

        <asp:GridView ID="GridView1" class="table table-bordered table-condensed table-striped" runat="server" AutoGenerateColumns="False" DataKeyNames="bud_id" DataSourceID="SqlDataSource1">
            <AlternatingRowStyle BackColor="DarkGray" />
            <Columns>
                <asp:BoundField DataField="bud_id" HeaderText="Budget ID" InsertVisible="False" ReadOnly="True" SortExpression="bud_id" />
                <asp:BoundField DataField="bud_moninc" HeaderText="Total Suggested" SortExpression="bud_moninc" />
                <asp:BoundField DataField="bud_tot" HeaderText="Total Actual" SortExpression="bud_tot" />
                <asp:BoundField DataField="bud_budget" HeaderText="Difference" SortExpression="bud_budget" />
                <asp:TemplateField HeaderText="Time" SortExpression="bud_time">
                    <EditItemTemplate>
                        <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("bud_time") %>'></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
     
                        <asp:HyperLink ID="HyperLink1" runat="server"  NavigateUrl='results.aspx'
                            Text='<%# Eval("bud_time", "{0}") %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT  [bud_id], [bud_moninc], [bud_tot], [bud_budget], [bud_time] FROM [details]"></asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>

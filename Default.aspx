﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="global/bootstrap.min.css" rel="stylesheet" />
    <link href="global/Style.css" rel="stylesheet" />
    <title>Project 2</title>
    <style type="text/css">
        .auto-style1 {
            width: 262px;
        }
        .auto-style2 {
            width: 468px;
        }
        .auto-style3 {
            width: 407px;
        }
        .auto-style4 {
            width: 179px;
        }
        .auto-style5 {
            width: 196px;
        }
        .auto-style6 {
            width: 213px;
        }
        .auto-style7 {
            width: 262px;
            height: 26px;
        }
        .auto-style8 {
            width: 179px;
            height: 26px;
        }
        .auto-style9 {
            width: 196px;
            height: 26px;
        }
        .auto-style10 {
            width: 213px;
            height: 26px;
        }
        </style>
    </head>
<body>
    <form id="form1" runat="server">
    <div>

    <h1>Project 2</h1>
        <img src="images/alex.jpg" alt="alexpic" class="img-circle" height="150px" width="130px"/>
        <img src="images/jee.jpg" alt="jeepic" class="img-circle" height="150px" width="130px" /><br />
        &nbsp&nbsp&nbsp&nbsp Alex Anderson &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp Jee Hoon Kim
        <br />
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href ="viewall.aspx">View all Processed Budgets</a></li>
                    </ul>
        <br />
 <br />
        <table id="table1" style="width: 45%;">
            <tr>
                <td class="auto-style2">
    
        <asp:Label ID="Label1" runat="server" Text=" &nbsp Yearly Gross Income:"></asp:Label>
                </td>
                <td class="auto-style3">
                    $<asp:TextBox ID="gross_txt" runat="server" Width="128px"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="gross_txt" ErrorMessage="yearly gross income required." Font-Bold="True" ForeColor="Red"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
    
        <asp:Label ID="Label2" runat="server" Text=" &nbsp Yearly Tithe Percentage (charity): "></asp:Label>
                </td>
                <td class="auto-style3">
                    &nbsp&nbsp<asp:TextBox ID="tithe_txt" runat="server" OnTextChanged="tithe_txt_TextChanged"></asp:TextBox>
                    
                    %<br /><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Yearly Tithe Percentage required." Font-Bold="True" ForeColor="Red" ControlToValidate="tithe_txt"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
    
                    &nbsp;</td>
                <td class="auto-style3">
                    <asp:Label ID="month_inc" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
    
                    &nbsp;</td>
                <td class="auto-style3">
                    <asp:Label ID="yearly_give" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
    
                    &nbsp;</td>
                <td class="auto-style3">
                    <asp:Label ID="yearly_tax" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
    
                    &nbsp;</td>
                <td class="auto-style3">
                    <asp:Label ID="yearly_net" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
    
                    &nbsp;</td>
                <td class="auto-style3">
                    <asp:Label ID="monthly_net" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
        </table>

        
&nbsp;<h2>Monthly Expenses</h2>
        <table id="table2" style="width:100%;" class="table table-bordered table-condensed table-striped">
            <tr>
                <td class="auto-style1">
                    Categories:</td>
                <td class="auto-style4">
                    Actual monthly spending:</td>
                <td class="auto-style5">
                    &nbsp;</td>
                <td class="auto-style6">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style1">
        <asp:Label ID="Label3" runat="server" Text="Housing:"></asp:Label>

                </td>
                <td class="auto-style4">
                    $<asp:TextBox ID="housing_txt" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Housing cost required." Font-Bold="True" ForeColor="Red" ControlToValidate="housing_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="housing_txt"></asp:CompareValidator>
                </td>
                <td class="auto-style5">
                    <asp:Label ID="house_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="house_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1" id="auto_txt1">
        <asp:Label ID="Label4" runat="server" Text="Food: "></asp:Label>

                </td>
                <td id="auto_txt1" class="auto-style4">
                    $<asp:TextBox ID="food_txt" runat="server" OnTextChanged="food_txt_TextChanged"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Food cost required." Font-Bold="True" ForeColor="Red" ControlToValidate="food_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="food_txt"></asp:CompareValidator>

                </td>
                <td id="auto_txt1" class="auto-style5">
                    <asp:Label ID="food_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td id="auto_txt1" class="auto-style6">
                    <asp:Label ID="food_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
        <asp:Label ID="Label5" runat="server" Text="Auto: "></asp:Label>

                </td>
                <td class="auto-style4">
                    $<asp:TextBox ID="auto_txt" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Auto cost required." ControlToValidate="auto_txt" Font-Bold="True" ForeColor="Red"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator3" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="auto_txt"></asp:CompareValidator>
                </td>
                <td class="auto-style5">
                    <asp:Label ID="auto_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="auto_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
        <asp:Label ID="Label6" runat="server" Text="Insurance: "></asp:Label>

                </td>
                <td class="auto-style4">
                    $<asp:TextBox ID="insur_txt" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Insurance cost required." Font-Bold="True" ForeColor="Red" ControlToValidate="insur_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="insur_txt"></asp:CompareValidator>

                </td>
                <td class="auto-style5">
                    <asp:Label ID="ins_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="ins_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
        <asp:Label ID="Label7" runat="server" Text="Debt: "></asp:Label>

                </td>
                <td class="auto-style4">
                    $<asp:TextBox ID="debt_txt" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="debt must not be blank." Font-Bold="True" ForeColor="Red" ControlToValidate="debt_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator5" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="debt_txt"></asp:CompareValidator>

                </td>
                <td class="auto-style5">
                    <asp:Label ID="debt_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="debt_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">
        <asp:Label ID="Label8" runat="server" Text="Entertainment: "></asp:Label>

                </td>
                <td class="auto-style8">
                    $<asp:TextBox ID="ent_txt" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Entertainment cost required." Font-Bold="True" ForeColor="Red" ControlToValidate="ent_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator6" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="ent_txt"></asp:CompareValidator>

                </td>
                <td class="auto-style9">
                    <asp:Label ID="ent_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style10">
                    <asp:Label ID="ent_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
        <asp:Label ID="Label9" runat="server" Text="Clothing: "></asp:Label>

                </td>
                <td class="auto-style4">
                    $<asp:TextBox ID="cloth_txt" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Clothing cost required." Font-Bold="True" ForeColor="Red" ControlToValidate="cloth_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator7" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="cloth_txt"></asp:CompareValidator>

                </td>
                <td class="auto-style5">
                    <asp:Label ID="cloth_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="cloth_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
        <asp:Label ID="Label10" runat="server" Text="Savings: "></asp:Label>

                </td>
                <td class="auto-style4">
                    $<asp:TextBox ID="savings_txt" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Savings amount required." Font-Bold="True" ForeColor="Red" ControlToValidate="savings_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator8" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="savings_txt"></asp:CompareValidator>

                </td>
                <td class="auto-style5">
                    <asp:Label ID="savings_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="save_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
        <asp:Label ID="Label11" runat="server" Text="Medical/Dental: "></asp:Label>

                </td>
                <td class="auto-style4">
                    $<asp:TextBox ID="meden_txt" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Medical/Dental cost required." Font-Bold="True" ForeColor="Red" ControlToValidate="meden_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator9" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="meden_txt"></asp:CompareValidator>

                </td>
                <td class="auto-style5">
                    <asp:Label ID="med_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="med_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
        <asp:Label ID="Label12" runat="server" Text="Miscellaneous: "></asp:Label>

                </td>
                <td class="auto-style4">
                    $<asp:TextBox ID="misc_txt" runat="server"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Miscellaneous cost required." Font-Bold="True" ForeColor="Red" ControlToValidate="misc_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator10" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="misc_txt"></asp:CompareValidator>

                </td>
                <td class="auto-style5">
                    <asp:Label ID="misc_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="misc_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
        <asp:Label ID="Label13" runat="server" Text="Investments: "></asp:Label>

                </td>
                <td class="auto-style4">
                    $<asp:TextBox ID="invest_txt" runat="server" OnTextChanged="invest_txt_TextChanged"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ErrorMessage="Investments amount required." Font-Bold="True" ForeColor="Red" ControlToValidate="invest_txt"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator11" runat="server" ErrorMessage="amount must be greater or equal to 0." Font-Bold="True" ForeColor="Red" Operator="GreaterThanEqual" Type="Double" ValueToCompare="0" ControlToValidate="invest_txt"></asp:CompareValidator>

                </td>
                <td class="auto-style5">
                    <asp:Label ID="inv_lbl" runat="server" Visible="False"></asp:Label>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="inv_diff" runat="server" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1">
                    Notes:</td>
                <td class="auto-style4">
                    <asp:TextBox ID="notes_txt" runat="server"></asp:TextBox>
                </td>
                <td class="auto-style5">
                    &nbsp;</td>
                <td class="auto-style6">
                    &nbsp;</td>
            </tr>
        </table>


        <br />
&nbsp;<asp:Label ID="bud_lbl" runat="server" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:Label ID="bud_tot" runat="server"></asp:Label>
        <br />
        <br />
        <center>
        <asp:Button ID="sub_btn" class="btn btn-info" runat="server" Text="Submit" OnClick="sub_btn_Click" />
&nbsp;
        <asp:Button ID="reset_btn" class="btn btn-warning" runat="server" Text="Reset" OnClick="reset_btn_Click" />
        <br />
            </center>
        </div>
    </form>
</body>
</html>

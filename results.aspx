﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="results.aspx.cs" Inherits="Default2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="global/bootstrap.min.css" rel="stylesheet" />
    <link href="global/Style.css" rel="stylesheet" />
    <title>Project 2</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

         <h1>Project 2</h1>
        <br />
    <a href="Default.aspx">Home</a>|<a href ="viewall.aspx">View all Processed Budgets</a>

<br />
        <br />


        <asp:DetailsView horizontalalign="center" ID="DetailsView1" class="table table-bordered table-condensed table-striped" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource2" Height="50px" Width="125px">
            <AlternatingRowStyle BackColor="DarkGray" />
            <Fields>
                <asp:BoundField DataField="bud_income" HeaderText="Yearly Gross Income:" SortExpression="bud_income" />
                <asp:BoundField DataField="bud_moninc" HeaderText="Monthly Gross Income:" SortExpression="bud_moninc" />
                <asp:BoundField DataField="bud_char" HeaderText="Yearly Charitable Giving:" SortExpression="bud_char" />
                <asp:BoundField DataField="bud_tax" HeaderText="Yearly Taxes:" SortExpression="bud_tax" />
                <asp:BoundField DataField="bud_ynetspend" HeaderText="Yearly Net Spendable:" SortExpression="bud_ynetspend" />
                <asp:BoundField DataField="bud_mnetspend" HeaderText="Monthly Net Spendable:" SortExpression="bud_mnetspend" />
            </Fields>
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT TOP(1) [bud_income] ,[bud_moninc], [bud_char], [bud_tax], [bud_ynetspend], [bud_mnetspend] FROM [details] ORDER BY [bud_id] DESC"></asp:SqlDataSource>
        <br />
        <br />
        <h1>Actual Monthly Spending</h1>
        <asp:DetailsView horizontalalign="center" ID="DetailsView2" class="table table-bordered table-condensed table-striped" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource1" Height="50px" Width="125px">
            <AlternatingRowStyle BackColor="DarkGray" />
            <Fields>
                <asp:BoundField DataField="bud_house" HeaderText="Actual Housing:" SortExpression="bud_house" />
                <asp:BoundField DataField="bud_food" HeaderText="Actual Food:" SortExpression="bud_food" />
                <asp:BoundField DataField="bud_auto" HeaderText="Actual Auto:" SortExpression="bud_auto" />
                <asp:BoundField DataField="bud_insurance" HeaderText="Actual Insurance:" SortExpression="bud_insurance" />
                <asp:BoundField DataField="bud_debt" HeaderText="Actual Debt:" SortExpression="bud_debt" />
                <asp:BoundField DataField="bud_ent" HeaderText="Actual Entertainment:" SortExpression="bud_ent" />
                <asp:BoundField DataField="bud_cloth" HeaderText="Actual Clothing:" SortExpression="bud_cloth" />
                <asp:BoundField DataField="bud_savings" HeaderText="Actual Savings:" SortExpression="bud_savings" />
                <asp:BoundField DataField="bud_med" HeaderText="Actual Medical:" SortExpression="bud_med" />
                <asp:BoundField DataField="bud_misc" HeaderText="Actual Misc:" SortExpression="bud_misc" />
                <asp:BoundField DataField="bud_invest" HeaderText="Actual Investments:" SortExpression="bud_invest" />
            </Fields>
        </asp:DetailsView>
        <br />
        <h1>Suggested Monthly Spending</h1>
        <asp:DetailsView horizontalalign="center" ID="DetailsView3" class="table table-bordered table-condensed table-striped" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource3" Height="50px" Width="125px">
            <AlternatingRowStyle BackColor="DarkGray" />
            <Fields>
                <asp:BoundField DataField="bud_sughouse" HeaderText="Suggested House:" SortExpression="bud_sughouse" />
                <asp:BoundField DataField="bud_sugfood" HeaderText="Suggested Food:" SortExpression="bud_sugfood" />
                <asp:BoundField DataField="bud_sugauto" HeaderText="Suggested Auto:" SortExpression="bud_sugauto" />
                <asp:BoundField DataField="bud_sugins" HeaderText="Suggested Insurance:" SortExpression="bud_sugins" />
                <asp:BoundField DataField="bud_sugdebt" HeaderText="Suggested Debt:" SortExpression="bud_sugdebt" />
                <asp:BoundField DataField="bud_sugent" HeaderText="Suggested Entertainment:" SortExpression="bud_sugent" />
                <asp:BoundField DataField="bud_sugcloth" HeaderText="Suggested Clothing:" SortExpression="bud_sugcloth" />
                <asp:BoundField DataField="bud_sugsavings" HeaderText="Suggested Savings:" SortExpression="bud_sugsavings" />
                <asp:BoundField DataField="bud_sugmed" HeaderText="Suggested Medical:" SortExpression="bud_sugmed" />
                <asp:BoundField DataField="bud_sugmisc" HeaderText="Suggested Misc:" SortExpression="bud_sugmisc" />
                <asp:BoundField DataField="bud_suginvest" HeaderText="Suggested Investments:" SortExpression="bud_suginvest" />
            </Fields>
        </asp:DetailsView>
        <br />
        <h1>Differences</h1>
        <asp:DetailsView horizontalalign="center" ID="DetailsView4" class="table table-bordered table-condensed table-striped" runat="server" AutoGenerateRows="False" DataSourceID="SqlDataSource4" Height="50px" Width="125px">
            <AlternatingRowStyle BackColor="DarkGray" />
            <Fields>
                <asp:BoundField DataField="bud_housediff" HeaderText="Housing Difference:" SortExpression="bud_housediff" />
                <asp:BoundField DataField="bud_fooddiff" HeaderText="Food Difference:" SortExpression="bud_fooddiff" />
                <asp:BoundField DataField="bud_autodiff" HeaderText="Auto Difference:" SortExpression="bud_autodiff" />
                <asp:BoundField DataField="bud_insdiff" HeaderText="Insurance Difference:" SortExpression="bud_insdiff" />
                <asp:BoundField DataField="bud_debtdiff" HeaderText="Debt Difference:" SortExpression="bud_debtdiff" />
                <asp:BoundField DataField="bud_entdiff" HeaderText="Entertainment Difference:" SortExpression="bud_entdiff" />
                <asp:BoundField DataField="bud_clothdiff" HeaderText="Clothing Difference:" SortExpression="bud_clothdiff" />
                <asp:BoundField DataField="bud_savingsdiff" HeaderText="Savings Difference:" SortExpression="bud_savingsdiff" />
                <asp:BoundField DataField="bud_meddiff" HeaderText="Medical Difference:" SortExpression="bud_meddiff" />
                <asp:BoundField DataField="bud_miscdiff" HeaderText="Misc Difference:" SortExpression="bud_miscdiff" />
                <asp:BoundField DataField="bud_investdiff" HeaderText="Investments Difference:" SortExpression="bud_investdiff" />
            </Fields>
        </asp:DetailsView>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT TOP(1) [bud_housediff] ,[bud_fooddiff], [bud_autodiff], [bud_insdiff], [bud_debtdiff], [bud_entdiff], [bud_clothdiff], [bud_savingsdiff], [bud_meddiff], [bud_miscdiff], [bud_investdiff] FROM [details] ORDER BY [bud_id] DESC"></asp:SqlDataSource>
        <br />
        <br />
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT TOP(1) [bud_sughouse] ,[bud_sugfood], [bud_sugauto], [bud_sugins], [bud_sugdebt], [bud_sugent], [bud_sugcloth], [bud_sugsavings], [bud_sugmed], [bud_sugmisc], [bud_suginvest] FROM [details] ORDER BY [bud_id] DESC"></asp:SqlDataSource>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT TOP(1) [bud_house] ,[bud_food], [bud_auto], [bud_insurance], [bud_debt], [bud_ent], [bud_cloth], [bud_savings], [bud_med], [bud_misc], [bud_invest] FROM [details] ORDER BY [bud_id] DESC"></asp:SqlDataSource>
    </div>
    </form>
</body>
</html>

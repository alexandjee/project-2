﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
    }


    protected void sub_btn_Click(object sender, EventArgs e)
    {


        double income = Convert.ToDouble(gross_txt.Text);
        double tithe = Convert.ToDouble(tithe_txt.Text);
        double house = Convert.ToDouble(housing_txt.Text);
        double food = Convert.ToDouble(food_txt.Text);
        double auto = Convert.ToDouble(auto_txt.Text);
        double insur = Convert.ToDouble(insur_txt.Text);
        double debt = Convert.ToDouble(debt_txt.Text);
        double ent = Convert.ToDouble(ent_txt.Text);
        double cloth = Convert.ToDouble(cloth_txt.Text);
        double save = Convert.ToDouble(savings_txt.Text);
        double med = Convert.ToDouble(meden_txt.Text);
        double misc = Convert.ToDouble(misc_txt.Text);
        double invest = Convert.ToDouble(invest_txt.Text);


        string monthres = Convert.ToString(Math.Round((income / 12), 2));
        month_inc.Text = monthres;

        double give = (Math.Round((income * (tithe/100))));
        string titheres = Convert.ToString(give);
        yearly_give.Text = titheres;

        if(income <= 7550)
        {
            string taxres = Convert.ToString(Math.Round((income * .1),2));
            yearly_tax.Text = Convert.ToString(taxres);
            double temp = Convert.ToDouble(taxres);
            string yearnetres = Convert.ToString(Math.Round((income - temp - give), 2));
            yearly_net.Text = yearnetres;
            double temp2 = Convert.ToDouble(yearnetres);
            string monthnetres = Convert.ToString(Math.Round((temp2 / 12), 2));
            monthly_net.Text = monthnetres;
            double housesug = (temp2 / 12);
            house_lbl.Text = Convert.ToString(Math.Round(housesug * .32));
            double housediffsug = (housesug * .32);
            house_diff.Text = Convert.ToString(Math.Round((housediffsug - house), 2));

            food_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double foodiffsug = (housesug * .13);
            food_diff.Text = Convert.ToString(Math.Round((foodiffsug - food), 2));

            auto_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double autodiffsug = (housesug * .13);
            auto_diff.Text = Convert.ToString(Math.Round((autodiffsug - auto), 2));

            ins_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double insdiffsug = (housesug * .05);
            ins_diff.Text = Convert.ToString(Math.Round((insdiffsug - insur), 2));

            debt_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double debtdiffsug = (housesug * .05);
            debt_diff.Text = Convert.ToString(Math.Round((debtdiffsug - debt), 2));

            ent_lbl.Text = Convert.ToString(Math.Round((housesug * .06), 2));
            double entdiffsug = (housesug * .06);
            ent_diff.Text = Convert.ToString(Math.Round((entdiffsug - ent), 2));

            cloth_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double clothdiffsug = (housesug * .05);
            cloth_diff.Text = Convert.ToString(Math.Round((clothdiffsug - cloth), 2));

            savings_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double savediffsug = (housesug * .05);
            save_diff.Text = Convert.ToString(Math.Round((savediffsug - save), 2));


            med_lbl.Text = Convert.ToString(Math.Round((housesug * .04), 2));
            double meddiffsug = (housesug * .04);
            med_diff.Text = Convert.ToString(Math.Round((meddiffsug - med), 2));

            misc_lbl.Text = Convert.ToString(Math.Round((housesug * .12), 2));
            double miscdiffsug = (housesug * .12);
            misc_diff.Text = Convert.ToString(Math.Round((miscdiffsug - misc), 2));

            inv_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double invdiffsug = (housesug * .05);
            inv_diff.Text = Convert.ToString(Math.Round((invdiffsug - invest), 2));

            double actualtotal = (house + food + auto + insur + debt + ent + cloth + save + med + misc + invest);
            double budget = (housesug - actualtotal);
            bud_lbl.Text = Convert.ToString(Math.Round((budget), 2));
            bud_tot.Text = Convert.ToString(Math.Round((actualtotal), 2));

        }
        else if (income <= 30650 && income > 7550)
        {
            string taxres = Convert.ToString(Math.Round((income * .15),2));
            yearly_tax.Text = Convert.ToString(taxres);
            double temp = Convert.ToDouble(taxres);
            string yearnetres = Convert.ToString(Math.Round((income - temp - give), 2));
            yearly_net.Text = yearnetres;
            double temp2 = Convert.ToDouble(yearnetres);
            string monthnetres = Convert.ToString(Math.Round((temp2 / 12), 2));
            monthly_net.Text = monthnetres;
            double housesug = (temp2 / 12);
            house_lbl.Text = Convert.ToString(Math.Round(housesug * .32));
            double housediffsug = (housesug * .32);
            house_diff.Text = Convert.ToString(Math.Round((housediffsug - house), 2));

            food_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double foodiffsug = (housesug * .13);
            food_diff.Text = Convert.ToString(Math.Round((foodiffsug - food), 2));

            auto_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double autodiffsug = (housesug * .13);
            auto_diff.Text = Convert.ToString(Math.Round((autodiffsug - auto), 2));

            ins_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double insdiffsug = (housesug * .05);
            ins_diff.Text = Convert.ToString(Math.Round((insdiffsug - insur), 2));

            debt_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double debtdiffsug = (housesug * .05);
            debt_diff.Text = Convert.ToString(Math.Round((debtdiffsug - debt), 2));

            ent_lbl.Text = Convert.ToString(Math.Round((housesug * .06), 2));
            double entdiffsug = (housesug * .06);
            ent_diff.Text = Convert.ToString(Math.Round((entdiffsug - ent), 2));

            cloth_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double clothdiffsug = (housesug * .05);
            cloth_diff.Text = Convert.ToString(Math.Round((clothdiffsug - cloth), 2));

            savings_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double savediffsug = (housesug * .05);
            save_diff.Text = Convert.ToString(Math.Round((savediffsug - save), 2));


            med_lbl.Text = Convert.ToString(Math.Round((housesug * .04), 2));
            double meddiffsug = (housesug * .04);
            med_diff.Text = Convert.ToString(Math.Round((meddiffsug - med), 2));

            misc_lbl.Text = Convert.ToString(Math.Round((housesug * .12), 2));
            double miscdiffsug = (housesug * .12);
            misc_diff.Text = Convert.ToString(Math.Round((miscdiffsug - misc), 2));

            inv_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double invdiffsug = (housesug * .05);
            inv_diff.Text = Convert.ToString(Math.Round((invdiffsug - invest), 2));

            double actualtotal = (house + food + auto + insur + debt + ent + cloth + save + med + misc + invest);
            double budget = (housesug - actualtotal);
            bud_lbl.Text = Convert.ToString(Math.Round((budget), 2));
            bud_tot.Text = Convert.ToString(Math.Round((actualtotal), 2));

        }
        else if (income <= 74200 && income > 30650)
        {
            string taxres = Convert.ToString(Math.Round((income * .25),2));
            yearly_tax.Text = Convert.ToString(taxres);
            double temp = Convert.ToDouble(taxres);
            string yearnetres = Convert.ToString(Math.Round((income - temp - give), 2));
            yearly_net.Text = yearnetres;
            double temp2 = Convert.ToDouble(yearnetres);
            string monthnetres = Convert.ToString(Math.Round((temp2 / 12), 2));
            monthly_net.Text = monthnetres;
            double housesug = (temp2 / 12);
            house_lbl.Text = Convert.ToString(Math.Round(housesug * .32));
            double housediffsug = (housesug * .32);
            house_diff.Text = Convert.ToString(Math.Round((housediffsug - house), 2));

            food_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double foodiffsug = (housesug * .13);
            food_diff.Text = Convert.ToString(Math.Round((foodiffsug - food), 2));

            auto_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double autodiffsug = (housesug * .13);
            auto_diff.Text = Convert.ToString(Math.Round((autodiffsug - auto), 2));

            ins_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double insdiffsug = (housesug * .05);
            ins_diff.Text = Convert.ToString(Math.Round((insdiffsug - insur), 2));

            debt_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double debtdiffsug = (housesug * .05);
            debt_diff.Text = Convert.ToString(Math.Round((debtdiffsug - debt), 2));

            ent_lbl.Text = Convert.ToString(Math.Round((housesug * .06), 2));
            double entdiffsug = (housesug * .06);
            ent_diff.Text = Convert.ToString(Math.Round((entdiffsug - ent), 2));

            cloth_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double clothdiffsug = (housesug * .05);
            cloth_diff.Text = Convert.ToString(Math.Round((clothdiffsug - cloth), 2));

            savings_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double savediffsug = (housesug * .05);
            save_diff.Text = Convert.ToString(Math.Round((savediffsug - save), 2));


            med_lbl.Text = Convert.ToString(Math.Round((housesug * .04), 2));
            double meddiffsug = (housesug * .04);
            med_diff.Text = Convert.ToString(Math.Round((meddiffsug - med), 2));

            misc_lbl.Text = Convert.ToString(Math.Round((housesug * .12), 2));
            double miscdiffsug = (housesug * .12);
            misc_diff.Text = Convert.ToString(Math.Round((miscdiffsug - misc), 2));

            inv_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double invdiffsug = (housesug * .05);
            inv_diff.Text = Convert.ToString(Math.Round((invdiffsug - invest), 2));

            double actualtotal = (house + food + auto + insur + debt + ent + cloth + save + med + misc + invest);
            double budget = (housesug - actualtotal);
            bud_lbl.Text = Convert.ToString(Math.Round((budget), 2));
            bud_tot.Text = Convert.ToString(Math.Round((actualtotal), 2));

        }
        else if (income <= 154800 && income > 74200)
        {
            string taxres = Convert.ToString(Math.Round((income * .28),2));
            yearly_tax.Text = Convert.ToString(taxres);
            double temp = Convert.ToDouble(taxres);
            string yearnetres = Convert.ToString(Math.Round((income - temp - give), 2));
            yearly_net.Text = yearnetres;
            double temp2 = Convert.ToDouble(yearnetres);
            string monthnetres = Convert.ToString(Math.Round((temp2 / 12), 2));
            monthly_net.Text = monthnetres;
            double housesug = (temp2 / 12);
            house_lbl.Text = Convert.ToString(Math.Round(housesug * .32));
            double housediffsug = (housesug * .32);
            house_diff.Text = Convert.ToString(Math.Round((housediffsug - house), 2));

            food_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double foodiffsug = (housesug * .13);
            food_diff.Text = Convert.ToString(Math.Round((foodiffsug - food), 2));

            auto_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double autodiffsug = (housesug * .13);
            auto_diff.Text = Convert.ToString(Math.Round((autodiffsug - auto), 2));

            ins_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double insdiffsug = (housesug * .05);
            ins_diff.Text = Convert.ToString(Math.Round((insdiffsug - insur), 2));

            debt_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double debtdiffsug = (housesug * .05);
            debt_diff.Text = Convert.ToString(Math.Round((debtdiffsug - debt), 2));

            ent_lbl.Text = Convert.ToString(Math.Round((housesug * .06), 2));
            double entdiffsug = (housesug * .06);
            ent_diff.Text = Convert.ToString(Math.Round((entdiffsug - ent), 2));

            cloth_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double clothdiffsug = (housesug * .05);
            cloth_diff.Text = Convert.ToString(Math.Round((clothdiffsug - cloth), 2));

            savings_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double savediffsug = (housesug * .05);
            save_diff.Text = Convert.ToString(Math.Round((savediffsug - save), 2));


            med_lbl.Text = Convert.ToString(Math.Round((housesug * .04), 2));
            double meddiffsug = (housesug * .04);
            med_diff.Text = Convert.ToString(Math.Round((meddiffsug - med), 2));

            misc_lbl.Text = Convert.ToString(Math.Round((housesug * .12), 2));
            double miscdiffsug = (housesug * .12);
            misc_diff.Text = Convert.ToString(Math.Round((miscdiffsug - misc), 2));

            inv_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double invdiffsug = (housesug * .05);
            inv_diff.Text = Convert.ToString(Math.Round((invdiffsug - invest), 2));

            double actualtotal = (house + food + auto + insur + debt + ent + cloth + save + med + misc + invest);
            double budget = (housesug - actualtotal);
            bud_lbl.Text = Convert.ToString(Math.Round((budget), 2));
            bud_tot.Text = Convert.ToString(Math.Round((actualtotal), 2));
        }
        else if (income <= 336550 && income > 154800)
        {
            string taxres = Convert.ToString(Math.Round((income * .33),2));
            yearly_tax.Text = Convert.ToString(taxres);
            double temp = Convert.ToDouble(taxres);
            string yearnetres = Convert.ToString(Math.Round((income - temp - give), 2));
            yearly_net.Text = yearnetres;
            double temp2 = Convert.ToDouble(yearnetres);
            string monthnetres = Convert.ToString(Math.Round((temp2 / 12), 2));
            monthly_net.Text = monthnetres;
            double housesug = (temp2 / 12);
            house_lbl.Text = Convert.ToString(Math.Round(housesug * .32));
            double housediffsug = (housesug * .32);
            house_diff.Text = Convert.ToString(Math.Round((housediffsug - house), 2));

            food_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double foodiffsug = (housesug * .13);
            food_diff.Text = Convert.ToString(Math.Round((foodiffsug - food), 2));

            auto_lbl.Text = Convert.ToString(Math.Round((housesug * .13), 2));
            double autodiffsug = (housesug * .13);
            auto_diff.Text = Convert.ToString(Math.Round((autodiffsug - auto), 2));

            ins_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double insdiffsug = (housesug * .05);
            ins_diff.Text = Convert.ToString(Math.Round((insdiffsug - insur), 2));

            debt_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double debtdiffsug = (housesug * .05);
            debt_diff.Text = Convert.ToString(Math.Round((debtdiffsug - debt), 2));

            ent_lbl.Text = Convert.ToString(Math.Round((housesug * .06), 2));
            double entdiffsug = (housesug * .06);
            ent_diff.Text = Convert.ToString(Math.Round((entdiffsug - ent), 2));

            cloth_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double clothdiffsug = (housesug * .05);
            cloth_diff.Text = Convert.ToString(Math.Round((clothdiffsug - cloth), 2));

            savings_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double savediffsug = (housesug * .05);
            save_diff.Text = Convert.ToString(Math.Round((savediffsug - save), 2));


            med_lbl.Text = Convert.ToString(Math.Round((housesug * .04), 2));
            double meddiffsug = (housesug * .04);
            med_diff.Text = Convert.ToString(Math.Round((meddiffsug - med), 2));

            misc_lbl.Text = Convert.ToString(Math.Round((housesug * .12), 2));
            double miscdiffsug = (housesug * .12);
            misc_diff.Text = Convert.ToString(Math.Round((miscdiffsug - misc), 2));

            inv_lbl.Text = Convert.ToString(Math.Round((housesug * .05), 2));
            double invdiffsug = (housesug * .05);
            inv_diff.Text = Convert.ToString(Math.Round((invdiffsug - invest), 2));

            double actualtotal = (house + food + auto + insur + debt + ent + cloth + save + med + misc + invest);
            double budget = (housesug - actualtotal);
            bud_lbl.Text = Convert.ToString(Math.Round((budget), 2));
            bud_tot.Text = Convert.ToString(Math.Round((actualtotal), 2));

        }
        else if (income > 336550) 
        {
            string taxres = Convert.ToString(Math.Round((income * .35),2));
            yearly_tax.Text = taxres;
            double temp = Convert.ToDouble(taxres);
            string yearnetres = Convert.ToString(Math.Round((income - temp - give),2));
            yearly_net.Text = yearnetres;
            double temp2 = Convert.ToDouble(yearnetres);
            string monthnetres = Convert.ToString(Math.Round((temp2 / 12),2));
            monthly_net.Text = monthnetres;
            double housesug = (temp2 / 12);
            house_lbl.Text = Convert.ToString(Math.Round(housesug * .32));
            double housediffsug = (housesug * .32);
            house_diff.Text = Convert.ToString(Math.Round((housediffsug - house),2));

            food_lbl.Text = Convert.ToString(Math.Round((housesug * .13),2));
            double foodiffsug = (housesug * .13);
            food_diff.Text = Convert.ToString(Math.Round((foodiffsug - food),2));

            auto_lbl.Text = Convert.ToString(Math.Round((housesug * .13),2));
            double autodiffsug = (housesug * .13);
            auto_diff.Text = Convert.ToString(Math.Round((autodiffsug - auto),2));

            ins_lbl.Text = Convert.ToString(Math.Round((housesug * .05),2));
            double insdiffsug = (housesug * .05);
            ins_diff.Text = Convert.ToString(Math.Round((insdiffsug - insur),2));

            debt_lbl.Text = Convert.ToString(Math.Round((housesug * .05),2));
            double debtdiffsug = (housesug * .05);
            debt_diff.Text = Convert.ToString(Math.Round((debtdiffsug - debt),2));

            ent_lbl.Text = Convert.ToString(Math.Round((housesug * .06),2));
            double entdiffsug = (housesug * .06);
            ent_diff.Text = Convert.ToString(Math.Round((entdiffsug - ent),2));

            cloth_lbl.Text = Convert.ToString(Math.Round((housesug * .05),2));
            double clothdiffsug = (housesug * .05);
            cloth_diff.Text = Convert.ToString(Math.Round((clothdiffsug - cloth),2));

            savings_lbl.Text = Convert.ToString(Math.Round((housesug * .05),2));
            double savediffsug = (housesug * .05);
            save_diff.Text = Convert.ToString(Math.Round((savediffsug - save),2));

           
            med_lbl.Text = Convert.ToString(Math.Round((housesug * .04),2));
            double meddiffsug = (housesug * .04);
            med_diff.Text = Convert.ToString(Math.Round((meddiffsug - med),2));

            misc_lbl.Text = Convert.ToString(Math.Round((housesug * .12),2));
            double miscdiffsug = (housesug * .12);
            misc_diff.Text = Convert.ToString(Math.Round((miscdiffsug - misc),2));

            inv_lbl.Text = Convert.ToString(Math.Round((housesug * .05),2));
            double invdiffsug = (housesug * .05);
            inv_diff.Text = Convert.ToString(Math.Round((invdiffsug - invest),2));

            double actualtotal = (house + food + auto + insur + debt + ent + cloth + save + med + misc + invest);
            double budget = (housesug - actualtotal);
            bud_lbl.Text = Convert.ToString(Math.Round((budget),2));
            bud_tot.Text = Convert.ToString(Math.Round((actualtotal), 2));
                

        }











        using (SqlConnection connection = new SqlConnection(@"Data Source=(LocalDB)\v11.0;AttachDbFilename=C:\Jeehoon\project-2\App_Data\budget.mdf;Integrated Security=True"))
       {
           SqlCommand test = new SqlCommand("INSERT INTO [details] ([bud_income], [bud_tithe], [bud_house], [bud_food], [bud_auto], [bud_insurance], [bud_debt], [bud_ent], [bud_cloth], [bud_savings], [bud_med], [bud_misc], [bud_invest], [bud_sughouse], [bud_sugfood], [bud_sugauto], [bud_sugins], [bud_sugdebt], [bud_sugent], [bud_sugcloth], [bud_sugsavings], [bud_sugmed], [bud_sugmisc], [bud_suginvest], [bud_housediff], [bud_fooddiff], [bud_autodiff], [bud_insdiff], [bud_debtdiff], [bud_entdiff], [bud_clothdiff], [bud_savingsdiff], [bud_meddiff], [bud_miscdiff], [bud_investdiff], [bud_budget], [bud_moninc], [bud_char], [bud_tax], [bud_ynetspend], [bud_mnetspend], [bud_tot], [bud_notes]) VALUES (@bud_income, @bud_tithe, @bud_house, @bud_food, @bud_auto, @bud_insurance, @bud_debt, @bud_ent, @bud_cloth, @bud_savings, @bud_med, @bud_misc, @bud_invest, @bud_sughouse, @bud_sugfood, @bud_sugauto, @bud_sugins, @bud_sugdebt, @bud_sugent, @bud_sugcloth, @bud_sugsavings, @bud_sugmed, @bud_sugmisc, @bud_suginvest, @bud_housediff, @bud_fooddiff, @bud_autodiff, @bud_insdiff, @bud_debtdiff, @bud_entdiff, @bud_clothdiff, @bud_savingsdiff, @bud_meddiff, @bud_miscdiff, @bud_investdiff, @bud_budget, @bud_moninc, @bud_char, @bud_tax, @bud_ynetspend, @bud_mnetspend, @bud_tot, @bud_notes)", connection);
           test.Parameters.AddWithValue("@bud_income", gross_txt.Text);
           test.Parameters.AddWithValue("@bud_tithe", tithe_txt.Text);
           test.Parameters.AddWithValue("@bud_house", housing_txt.Text);
           test.Parameters.AddWithValue("@bud_food", food_txt.Text);
           test.Parameters.AddWithValue("@bud_auto", auto_txt.Text);
           test.Parameters.AddWithValue("@bud_insurance", insur_txt.Text);
           test.Parameters.AddWithValue("@bud_debt", debt_txt.Text);
           test.Parameters.AddWithValue("@bud_ent", ent_txt.Text);
           test.Parameters.AddWithValue("@bud_cloth", cloth_txt.Text);
           test.Parameters.AddWithValue("@bud_savings", savings_txt.Text);
           test.Parameters.AddWithValue("@bud_med", meden_txt.Text);
           test.Parameters.AddWithValue("@bud_misc", misc_txt.Text);
           test.Parameters.AddWithValue("@bud_invest", invest_txt.Text);
           test.Parameters.AddWithValue("@bud_notes", notes_txt.Text);

           test.Parameters.AddWithValue("@bud_sughouse", house_lbl.Text);
           test.Parameters.AddWithValue("@bud_sugfood", food_lbl.Text);
           test.Parameters.AddWithValue("@bud_sugauto", auto_lbl.Text);
           test.Parameters.AddWithValue("@bud_sugins", ins_lbl.Text);
           test.Parameters.AddWithValue("@bud_sugdebt", debt_lbl.Text);
           test.Parameters.AddWithValue("@bud_sugent", ent_lbl.Text);
           test.Parameters.AddWithValue("@bud_sugcloth", cloth_lbl.Text);
           test.Parameters.AddWithValue("@bud_sugsavings", savings_lbl.Text);
           test.Parameters.AddWithValue("@bud_sugmed", med_lbl.Text);
           test.Parameters.AddWithValue("@bud_sugmisc", misc_lbl.Text);
           test.Parameters.AddWithValue("@bud_suginvest", inv_lbl.Text);
         
           test.Parameters.AddWithValue("@bud_housediff", house_diff.Text);
           test.Parameters.AddWithValue("@bud_fooddiff", food_diff.Text);
           test.Parameters.AddWithValue("@bud_autodiff", auto_diff.Text);
           test.Parameters.AddWithValue("@bud_insdiff", ins_diff.Text);
           test.Parameters.AddWithValue("@bud_debtdiff", debt_diff.Text);
           test.Parameters.AddWithValue("@bud_entdiff", ent_diff.Text);
           test.Parameters.AddWithValue("@bud_clothdiff", cloth_diff.Text);
           test.Parameters.AddWithValue("@bud_savingsdiff", save_diff.Text);
           test.Parameters.AddWithValue("@bud_meddiff", med_diff.Text);
           test.Parameters.AddWithValue("@bud_miscdiff", misc_diff.Text);
           test.Parameters.AddWithValue("@bud_investdiff", inv_diff.Text);

           test.Parameters.AddWithValue("@bud_budget", bud_lbl.Text);
           test.Parameters.AddWithValue("@bud_moninc", month_inc.Text);
           test.Parameters.AddWithValue("@bud_char", yearly_give.Text);
           test.Parameters.AddWithValue("@bud_tax", yearly_tax.Text);
           test.Parameters.AddWithValue("@bud_ynetspend", yearly_net.Text);
           test.Parameters.AddWithValue("@bud_mnetspend", monthly_net.Text);
           test.Parameters.AddWithValue("@bud_tot", bud_tot.Text);

           connection.Open();
           test.ExecuteNonQuery();
           connection.Close();
       }

      Server.Transfer("results.aspx", true);
    }






    protected void tithe_txt_TextChanged(object sender, EventArgs e)
    {

    }
    protected void food_txt_TextChanged(object sender, EventArgs e)
    {

    }
    protected void invest_txt_TextChanged(object sender, EventArgs e)
    {

    }
    protected void reset_btn_Click(object sender, EventArgs e)
    {
        gross_txt.Text = "";
        tithe_txt.Text = "";
        housing_txt.Text = "";
        food_txt.Text = "";
        auto_txt.Text = "";
        insur_txt.Text = "";
        debt_txt.Text = "";
        ent_txt.Text = "";
        cloth_txt.Text = "";
        savings_txt.Text = "";
        meden_txt.Text = "";
        misc_txt.Text = "";
        invest_txt.Text = "";
        notes_txt.Text = "";
    }
}